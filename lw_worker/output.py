#!/usr/bin/env python
#-*- coding: utf-8 -*-

from __future__ import print_function
import time
import random
import json


class BufferedOutput(object):
    def __init__(self):
        self.buffer_limit = 128
        self.buffer_byte_limit = 10000000
        self.flush_time = 60
        self.buffer = []
        self.buffer_size = 0
        self.buffer_byte_size = 0
        self.buffer_flush_time = int(time.time()) + self.flush_time

    def push(self, msg):

        msg_sizeof = len(json.dumps(msg))
        if (msg_sizeof + self.buffer_byte_size > self.buffer_byte_limit):
            if self.buffer_size > 1:
                self.flush()
            self.set_buffer()

        self.buffer.append(msg)
        self.buffer_byte_size += msg_sizeof
        self.buffer_size += 1

        if (self.buffer_size > self.buffer_limit or
            int(time.time()) > self.buffer_flush_time):
            self.flush()
            self.set_buffer()

    def set_buffer(self):
        self.buffer = []
        self.buffer_size = 0
        self.buffer_byte_size = 0
        self.buffer_flush_time = int(time.time()) + self.flush_time

    def flush(self):
        pass


class GoogleCloudOutput(BufferedOutput):
    def __init__(self, client):
        super(GoogleCloudOutput, self).__init__()
        self.buffer_byte_limit = 80000
        self.client = client

    def flush(self, retry=2):
        body = {"entries": self.buffer}
        try:
            resp = self.client.entries().write(body=body).execute()
        except Exception as e:
            print(e)
            if retry > 1024:
                raise Exception(
                    "HTTPError: retry connect to server more than 10 times")
            time.sleep(retry + random.randint(1, 10))
            self.flush(retry << 1)


class DebugOutput(BufferedOutput):
    def __init__(self, *args, **kwargs):
        super(DebugOutput, self).__init__()
        self.buffer_byte_limit = 100000

    def flush(self):
        print(self.buffer)
