#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import gcs_oauth2_boto_plugin
from collections import namedtuple

Config = namedtuple("Config", ["project_id", "google_storage"])


def prepare():
    """
    Set fallback client id and secret. Return global variables

    Args:
    Return:
      Config
    """
    with open("config.json") as config:
        json_config = json.loads(config.read())
        client_id = json_config["installed"]["client_id"]
        client_secret = json_config["installed"]["client_secret"]
        project_id = json_config["installed"]["project_id"]

    print(client_id, client_secret)
    gcs_oauth2_boto_plugin.SetFallbackClientIdAndSecret(client_id,
                                                        client_secret)
    return Config(project_id=project_id, google_storage="gs")
